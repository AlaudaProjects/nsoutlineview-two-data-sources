//
//  OutlineViewController.h
//  NSOutlineView_Simple_Two_Dictionaries
//
//  Created by AlaudaProjects on 16.04.2012.
//  Copyright (c) 2003-2013 AlaudaProjects All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SecondOutlineViewDataSource.h"

@interface OutlineViewController : NSOutlineView <NSOutlineViewDataSource, NSOutlineViewDelegate> {
    NSDictionary *firstParent;
    NSDictionary *secondParent;
    NSArray *list;
    SecondOutlineViewDataSource *secondSource;
}
@property (nonatomic, retain) IBOutlet NSOutlineView *myOutlineView;
@property (nonatomic, retain) IBOutlet NSButton *firstCheckbox;
@property (nonatomic, retain) IBOutlet NSButton *secondCheckbox;

- (IBAction)checkState:(id)sender;


@end
