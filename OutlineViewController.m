//
//  OutlineViewController.m
//  NSOutlineView_Simple_Two_Dictionaries
//
//  Created by AlaudaProjects on 16.04.2012.
//  Copyright (c) 2003-2013 AlaudaProjects All rights reserved.
//  Instruments:
//  http://developer.apple.com/library/ios/#documentation/DeveloperTools/Conceptual/InstrumentsUserGuide/UsingtheTraceDocument/UsingtheTraceDocument.html
//
//  http://disanji.net/iOS_Doc/#documentation/DeveloperTools/Conceptual/XcodeDebugging/220-Viewing_Variables_and_Memory/variables_and_memory.html

#import "OutlineViewController.h"

static NSUserDefaults *ud1 = nil;
//static NSUserDefaults *ud = nil;

@interface CheckForJames : NSObject

- (BOOL)_checkForJames:(id)item;

@end

@implementation CheckForJames

- (BOOL)_checkForJames:(id)item
{
    if ([item isKindOfClass:[NSDictionary class]]) {
        return [[item objectForKey:@"parent"] isEqualToString:@"James"] ? YES : NO;
    }
    return YES;
}

@end

@implementation OutlineViewController

/**
 * example on how to use +(void)initialize
 */
+ (void)initialize
{
    static BOOL initialized = NO;
    if (!initialized) {
        initialized = YES;
        ud1 = [NSUserDefaults standardUserDefaults];
    }
}

- (id)init
{
    if (self = [super init]) {
        // use ud if you want to initialize via init
        // ud = [NSUserDefaults standardUserDefaults];
        firstParent = [[NSDictionary alloc] initWithObjectsAndKeys:@"James",@"parent",[NSArray arrayWithObjects:@"Mary",@"Charlie", nil],@"children", nil];
        
        secondParent = [[NSDictionary alloc] initWithObjectsAndKeys:@"Elisabeth",@"parent",[NSArray arrayWithObjects:@"Jimmie",@"Kate", nil],@"children", nil];
        
        list = [[NSArray arrayWithObjects:firstParent,secondParent, nil] retain];
        
    }
    return self;
}

- (void)awakeFromNib
{
    DLog(@"%@", list);
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"firstCheckbox"]) {
        DLog(@"observed: %@", keyPath);
    }
}

#pragma mark -
#pragma mark NSOutlineView data source delegate methods

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item
{
    if ([item isKindOfClass:[NSDictionary class]] || [item isKindOfClass:[NSArray class]]) {
        return YES;
    }else {
        return NO;
    }
}

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item
{
    
    if (item == nil) { //item is nil when the outline view wants to inquire for root level items
        return [list count];
    }
    
    if ([item isKindOfClass:[NSDictionary class]]) {
        return [[item objectForKey:@"children"] count];
    }
    
    return 0;
}

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item
{
    
    if (item == nil) { //item is nil when the outline view wants to inquire for root level items
        return [list objectAtIndex:index];
    }
    
    if ([item isKindOfClass:[NSDictionary class]]) {
        return [[item objectForKey:@"children"] objectAtIndex:index];
    }
    
    return nil;
}

- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)theColumn byItem:(id)item
{
    
    if ([[theColumn identifier] isEqualToString:@"children"]) {
        if ([item isKindOfClass:[NSDictionary class]]) {
            return [NSString stringWithFormat:@"%li kids",[[item objectForKey:@"children"] count]];
            
        }
        return item;
    } else {
        if ([item isKindOfClass:[NSDictionary class]]) {
            return [item objectForKey:@"parent"];
        }
    }
    
    return nil;
}

- (IBAction)checkState:(id)sender
{
    DLog(@"Checkbox state: %@", [sender state] == 0 ? @"NO" : @"YES");
    
}


- (BOOL)allowJamesToExpand
{
    return (BOOL)[_firstCheckbox state];
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldExpandItem:(id)item
{
    NSLog(@"%@", [ud1 boolForKey:@"kFirstCheckbox"] ? @"YES" : @"NO");
    NSLog(@"%@", [ud1 boolForKey:@"kSecondCheckbox"] ? @"YES" : @"NO");
    
    if ([[item objectForKey:@"parent"] isEqualToString:@"James"] && [ud1 boolForKey:@"kFirstCheckbox"])
        return YES;
    
    if ([[item objectForKey:@"parent"] isEqualToString:@"Elisabeth"] && [ud1 boolForKey:@"kSecondCheckbox"])
        return YES;
    
    return NO;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldCollapseItem:(id)item
{
    return YES;
}

- (void)dealloc
{
    [firstParent release];
    [secondParent release];
    [secondSource release];
    [super dealloc];
}

@end
