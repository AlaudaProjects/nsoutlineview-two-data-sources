//
//  AppDelegate.m
//  NSOutlineView_Simple_Two_Dictionaries
//
//  Created by AlaudaProjects on 16.04.2012.
//  Copyright (c) 2003-2013 AlaudaProjects All rights reserved.
//

#import "AppDelegate.h"

static NSUserDefaults *ud = nil;

@implementation AppDelegate

OutlineViewController *ovc;
SecondOutlineViewDataSource *sovc;

@synthesize window = _window;
@synthesize myOutlineView = _myOutlineView;

- (id)init
{
    if (self = [super init]) {
        ovc = [[OutlineViewController alloc] init];
        ud = [NSUserDefaults standardUserDefaults];
        [self setNewDataSource:(id<NSOutlineViewDataSource>)ovc];
        [self setNewDelegate:(id<NSOutlineViewDelegate>)ovc];
    }
    
    return self;
}


- (void)dealloc
{
    [super dealloc];
    [sovc release];
    [ovc release];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application

}

- (void)setNewDelegate:(id<NSOutlineViewDelegate>)newDelegate
{
    if (oldDelegate != newDelegate) {
        if (oldDelegate)
            [oldDelegate release];
        oldDelegate = [newDelegate retain];
    }
}

- (id<NSOutlineViewDelegate>)getNewDelegate
{
    return oldDelegate;
}

- (void)setNewDataSource:(id<NSOutlineViewDataSource>)newDataSource
{
    if (oldDataSource != newDataSource) {
        if (oldDataSource)
            [oldDataSource release];
        oldDataSource = [newDataSource retain];
    }
}

- (id<NSOutlineViewDataSource>)getNewDataSource
{
    return oldDataSource;
}

- (void)awakeFromNib
{
    [_myOutlineView setDataSource:[self getNewDataSource]];
    [_myOutlineView setDelegate:[self getNewDelegate]];
    
    [_firstCheckbox setTitle:@"Expand/ Collapse James Node"];
    [_secondCheckbox setTitle:@"Expand/ Collapse Elisabeth Node"];
    
    [_changeDelegate setTitle:@"Second Data Source"];
    [_changeDelegate sizeToFit];    
    
    [AppDelegate addObserver:_myOutlineView forKeyPath:@"firstCheckbox" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:NULL];
}

- (IBAction)toggleDataSource:(id)sender
{
    [_myOutlineView setDataSource:nil];
    [_myOutlineView setDelegate:nil];
    
   
#ifdef DEBUG
    NSLog(@"Data Source toggle");
#endif
    
    if ([sender state] == NSOnState) {
        [_firstCheckbox setTitle:@"Expand/ Collapse Mac Node"];
        [_secondCheckbox setTitle:@"Expand/ Collapse PC Node"];
        [_changeDelegate setTitle:@"First Data Source"];
        [_changeDelegate sizeToFit];        
        sovc = [[SecondOutlineViewDataSource alloc] init];
        [self setNewDelegate:(id<NSOutlineViewDelegate>)sovc];
        [self setNewDataSource:(id<NSOutlineViewDataSource>)sovc];
        //[_myOutlineView setDelegate:[self getNewDelegate]];
        //[_myOutlineView setDataSource:[self getNewDataSource]];
        [_myOutlineView setDelegate:(id<NSOutlineViewDelegate>)sovc];
        [_myOutlineView setDataSource:(id<NSOutlineViewDataSource>)sovc];

        [_myOutlineView reloadData];
        
#ifdef DEBUG
        NSLog(@"On state with source: %@", [[_myOutlineView dataSource] description]);
#endif


    } else {
        [_firstCheckbox setTitle:@"Expand James Node"];
        [_secondCheckbox setTitle:@"Expand Elisabeth Node"];
        [_changeDelegate setTitle:@"Second Data Source"];
        [_changeDelegate sizeToFit];
        ovc = [[OutlineViewController alloc] init];
        [self setNewDelegate:(id<NSOutlineViewDelegate>)ovc];
        [self setNewDataSource:(id<NSOutlineViewDataSource>)ovc];
        //[_myOutlineView setDelegate:[self getNewDelegate]];
        //[_myOutlineView setDataSource:[self getNewDataSource]];
        [_myOutlineView setDelegate:(id<NSOutlineViewDelegate>)ovc];
        [_myOutlineView setDataSource:(id<NSOutlineViewDataSource>)ovc];
        [_myOutlineView reloadData];
        
        

#ifdef DEBUG
        NSLog(@"Off state with source: %@", [[_myOutlineView dataSource] description]);
#endif
    }
}

@end
