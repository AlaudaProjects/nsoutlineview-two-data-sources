//
//  main.m
//  NSOutlineView_Simple_Two_Dictionaries
//
//  Created by AlaudaProjects on 16.04.2012.
//  Copyright (c) 2003-2013 AlaudaProjects All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
