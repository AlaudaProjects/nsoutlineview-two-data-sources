//
//  SecondOutlineViewDataSource.h
//  NSOutlineView_Simple_Two_Dictionaries
//
//  Created by AlaudaProjects on 31.01.2013.
//  Copyright (c) 2003-2013 AlaudaProjects All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SecondOutlineViewDataSource : NSObject <NSOutlineViewDelegate, NSOutlineViewDataSource> {
    NSDictionary *firstParent;
    NSDictionary *secondParent;
    NSArray *list;
}

@property (nonatomic, retain) IBOutlet NSOutlineView *myOutlineView;

@end
