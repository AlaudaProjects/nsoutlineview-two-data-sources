//
//  AppDelegate.h
//  NSOutlineView_Simple_Two_Dictionaries
//
//  Created by AlaudaProjects on 16.04.2012.
//  Copyright (c) 2003-2013 AlaudaProjects All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "OutlineViewController.h"
#import "SecondOutlineViewDataSource.h"

@interface AppDelegate : NSObject <NSApplicationDelegate> {
    id<NSOutlineViewDataSource> oldDataSource;
    id<NSOutlineViewDelegate> oldDelegate;
}

@property (assign) IBOutlet NSWindow *window;
@property (nonatomic, retain) IBOutlet NSOutlineView *myOutlineView;
@property (nonatomic, retain) IBOutlet NSButton *firstCheckbox;
@property (nonatomic, retain) IBOutlet NSButton *secondCheckbox;
@property (nonatomic, retain) IBOutlet NSButton *changeDelegate;


- (IBAction)toggleDataSource:(id)sender;

- (void)setNewDelegate:(id<NSOutlineViewDelegate>)newDelegate;
- (id<NSOutlineViewDelegate>)getNewDelegate;

- (void)setNewDataSource:(id<NSOutlineViewDataSource>)newDataSource;
- (id<NSOutlineViewDataSource>)getNewDataSource;

@end
