//
//  SecondOutlineViewDataSource.m
//  NSOutlineView_Simple_Two_Dictionaries
//
//  Created by AlaudaProjects on 31.01.2013.
//  Copyright (c) 2003-2013 AlaudaProjects All rights reserved.
//

#import "SecondOutlineViewDataSource.h"

static NSUserDefaults *ud1 = nil;
//static NSUserDefaults *ud;

@implementation SecondOutlineViewDataSource

/**
 * example on how to use +(void)initialize
 */
+ (void)initialize
{
    static BOOL initialized = NO;
    if (!initialized) {
        initialized = YES;
        ud1 = [NSUserDefaults standardUserDefaults];
    }
}

- (id)init
{
    if (self = [super init]) {
        // use ud if you want to initialize via init
        // ud = [NSUserDefaults standardUserDefaults];
        firstParent = [[[NSDictionary alloc] initWithObjectsAndKeys:@"Mac",@"computers",[NSArray arrayWithObjects:@"eMac",@"iMac", @"MacBook", @"iMac", @"MacBook Pro", nil],@"children", nil] retain];
        
        secondParent = [[[NSDictionary alloc] initWithObjectsAndKeys:@"PC",@"computers",[NSArray arrayWithObjects:@"Toshiba", @"Lenovo", @"DELL", @"HP", nil],@"children", nil] retain];
        
        list = [[NSArray arrayWithObjects:firstParent,secondParent, nil] retain];
        
    }
    return self;
}

- (void)awakeFromNib
{
    DLog(@"%@", list);
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item
{
    if ([item isKindOfClass:[NSDictionary class]] || [item isKindOfClass:[NSArray class]]) {
        [firstParent release];
        return YES;
    }else {
        return NO;
    }
}


- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item
{
    if (item == nil) { //item is nil when the outline view wants to inquire for root level items
        return [list count];
    }
    
    if ([item isKindOfClass:[NSDictionary class]]) {
        return [[item objectForKey:@"children"] count];
    }
    
    return 0;
}

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item
{
    if (item == nil) { //item is nil when the outline view wants to inquire for root level items
        return [list objectAtIndex:index];
    }
    
    if ([item isKindOfClass:[NSDictionary class]]) {
        return [[item objectForKey:@"children"] objectAtIndex:index];
    }
    
    return nil;
}

- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item

{
    if ([[tableColumn identifier] isEqualToString:@"children"]) {
        if ([item isKindOfClass:[NSDictionary class]]) {
            return [NSString stringWithFormat:@"%li computers",[[item objectForKey:@"children"] count]];
            
        }
        return item;
    } else {
        if ([item isKindOfClass:[NSDictionary class]]) {
            return [item objectForKey:@"computers"];
        }
    }
    
    return nil;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldExpandItem:(id)item
{
    DLog(@"%@", [ud1 boolForKey:@"kFirstCheckbox"] ? @"YES" : @"NO");
    DLog(@"%@", [ud1 boolForKey:@"kSecondCheckbox"] ? @"YES" : @"NO");
    
    if ([[item objectForKey:@"computers"] isEqualToString:@"Mac"] && [ud1 boolForKey:@"kFirstCheckbox"])
        return YES;
    
    if ([[item objectForKey:@"computers"] isEqualToString:@"PC"] && [ud1 boolForKey:@"kSecondCheckbox"])
        return YES;
    
    return NO;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldCollapseItem:(id)item
{
    return YES;
}

- (void)dealloc
{
    [firstParent release];
    [secondParent release];
    [list release];
    [super dealloc];
}

@end
